<?php namespace App\Models;

use CodeIgniter\Model;

class OstosModel extends Model {
  protected $table = 'ostos';

  protected $allowedFields = ['kuvaus'];

  public function haeOstokset() {
    return $this->findAll();
  }
}