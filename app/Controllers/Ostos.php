<?php namespace App\Controllers;

use App\Models\OstosModel;

class Ostos extends BaseController
{
	public function index()
	{
    $ostos_model = new OstosModel();
    $data['ostokset'] = $ostos_model->haeOstokset();
    $data['maara'] = count($data['ostokset']);
		return view('ostos_view',$data);
  }
  
  public function lisaa() {
    $ostos_model = new OstosModel();
    $ostos_model->save([
      'kuvaus' => $this->request->getVar('kuvaus')
    ]);
    return redirect('ostos');
  }
}
